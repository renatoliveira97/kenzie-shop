import './style.css';

import formatValue from '../../utils/formatValue';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { removeFromCartThunk } from '../../store/modules/cart/thunks';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const Cart = () => {

    const dispatch = useDispatch();

    const cart = useSelector((state) => state.cart);

    const history = useHistory();

    const goToHome = () => {
        history.push('/');
    }

    return (
        <div className='cart'>
            <div className="cart-container">
                <div className="cart-header">
                    <h4>Produto</h4>
                </div>
                <hr></hr>
                {cart.map((item) => (
                    <li className='cart-li' key={item.id}>
                        <figure>
                            <img className='cart-img' src={item.image} alt={item.name}/>
                        </figure>
                        <div className='cart-item-info'>
                            <strong>{item.name}</strong>
                            <span>{item.priceFormatted}</span>
                        </div>
                        <DeleteForeverIcon className='cart-delete-item' onClick={() => dispatch(removeFromCartThunk(item))}/>
                    </li>
                ))}
            </div>
            <div className="cart-total">
                <h2>Resumo do pedido</h2>
                <hr></hr>
                {cart.length === 0 ? (
                    <div className='cart-no-items'>
                        <span><strong>Não há itens no carrinho, que tal acessar a loja para comprar algo?</strong></span>
                        <button type='button' onClick={goToHome}>Acessar a loja</button>
                    </div>
                ) : (
                    <div className='cart-with-items'>
                        <div className='cart-with-items-div'>
                            <h3>{cart.length} items no carrinho</h3>
                            <span>{formatValue(cart.reduce((x, y) => y.price + x, 0).toFixed(2))}</span>
                        </div>                        
                        <button type='button'>Finalizar compra</button>
                    </div>
                )}
            </div>
        </div>
        
    );
}

export default Cart;