import './style.css';

import formatValue from '../../utils/formatValue';
import CircularProgress from '@material-ui/core/CircularProgress';

import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { addToCartThunk } from '../../store/modules/cart/thunks';

import Products from '../../store/modules/products';

const Home = () => {

    const dispatch = useDispatch();

    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);

    const loadProducts = () => {
        const data = Products.map((product) => ({
            ...product,
            priceFormatted: formatValue(product.price),
        }));

        setLoading(false);
        setProducts(data);
    }

    useEffect(() => {
        loadProducts();
    }, []);

    return (
        <div className='container'>
            {loading ? (
                <CircularProgress size={50}/>
            ) : (
                <div className='product-list'>
                    {products.map((product) => (
                        <li key={product.id}>
                            <figure>
                                <img src={product.image} alt={product.name}/>
                            </figure>
                            <hr></hr>
                            <div className='product-name'>
                                <strong>{product.name}</strong>
                            </div>                            
                            <div>
                                <span>{product.priceFormatted}</span>
                            </div>
                            <button type='button' onClick={() => dispatch(addToCartThunk(product))}>
                                <span>Adicionar ao carrinho</span>
                            </button>
                        </li>
                    ))}
                </div>
            )}
        </div>
    );
}

export default Home;