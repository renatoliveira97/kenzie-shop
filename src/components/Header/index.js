import './style.css';

import { Badge } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';

import { useSelector } from 'react-redux';

const Header = () => {

    const cart = useSelector((state) => state.cart);

    const history = useHistory();

    const goToCart = () => {
        history.push('/cart');
    }

    const goToHome = () => {
        history.push('/');
    }

    return (
        <header>
            <div className='cabecalho'>
                <h1 onClick={goToHome}>Kenzie Shop</h1>
                <Badge className='carrinho' badgeContent={cart.length} color='error' onClick={goToCart}>
                    <ShoppingCartIcon/>
                </Badge>
            </div>   
            <hr></hr>         
        </header>
    );
}

export default Header;