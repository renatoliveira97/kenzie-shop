const defaultState = JSON.parse(localStorage.getItem('cart')) || [];

const cartReducer = (state = defaultState, action) => {

    const { product } = action;

    switch (action.type) {
        case '@cart/ADD':            
            return [...state, product];

        case '@cart/Remove':
            return state.filter((item) => item.id !== product.id);

        default:
            return state;
    }
}

export default cartReducer;